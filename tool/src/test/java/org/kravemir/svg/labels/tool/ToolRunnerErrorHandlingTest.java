package org.kravemir.svg.labels.tool;

import org.apache.commons.io.FileUtils;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemErrRule;
import org.junit.rules.TemporaryFolder;
import org.kravemir.svg.labels.TemplateResoures;
import org.kravemir.svg.labels.util.ListBuilder;
import org.w3c.dom.Node;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.kravemir.svg.labels.TemplateResoures.*;
import static org.kravemir.svg.labels.matcher.NodesMatchingXPath.nodesMatchingXPath;
import static org.kravemir.svg.labels.tool.FileContainsSvgMatcher.isSvgFileThat;

public class ToolRunnerErrorHandlingTest extends AbstractToolRunnerTest{

    @Test
    public void testRenderWithoutInstance() throws IOException {
        File inputFile = folder.newFile("non-existing-file");
        inputFile.delete();

        ToolRunner.main(new String[]{
                "tile",
                "--paper-size", "210", "297",
                "--label-offset", "0", "0",
                "--label-size", "65", "26.5",
                "--label-delta", "0", "0",
                inputFile.getAbsolutePath(),
                outputFile.getAbsolutePath()
        });

        assertThat(systemErrRule.getLog(), is("Input for SVG_SOURCE \"" + inputFile.getAbsolutePath() + "\" not found\n"));
    }
}
