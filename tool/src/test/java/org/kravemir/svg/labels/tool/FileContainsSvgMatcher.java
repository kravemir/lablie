package org.kravemir.svg.labels.tool;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.util.XMLResourceDescriptor;
import org.apache.commons.io.FileUtils;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.w3c.dom.svg.SVGDocument;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class FileContainsSvgMatcher extends TypeSafeDiagnosingMatcher<File> {

    private final Matcher<? super SVGDocument> contentMatcher;

    private FileContainsSvgMatcher(Matcher<? super SVGDocument> contentMatcher) {
        this.contentMatcher = contentMatcher;
    }

    @Override
    protected boolean matchesSafely(File file, Description mismatchDescription) {
        if (!file.exists()) {
            mismatchDescription
                    .appendText("file ")
                    .appendValue(file.getAbsolutePath())
                    .appendText(" doesn't exist");

            return false;
        }

        String s = null;
        try {
            s = FileUtils.readFileToString(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String parser = XMLResourceDescriptor.getXMLParserClassName();
        SAXSVGDocumentFactory factory = new SAXSVGDocumentFactory(parser);
        SVGDocument svg;
        try {
            svg = factory.createSVGDocument("", new ByteArrayInputStream(s.getBytes(StandardCharsets.UTF_8)));
        } catch (IOException e) {
            mismatchDescription
                    .appendText("failed to parse file, because: ")
                    .appendValue(e.getMessage());

            return false;
        }

        if(contentMatcher != null) {
            if(contentMatcher.matches(svg)) {
                return  true;
            } else {
                contentMatcher.describeMismatch(svg, mismatchDescription);
                return false;
            }
        }

        return true;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("is valid SVG file");

        if(contentMatcher != null) {
            description
                    .appendText(", that ")
                    .appendDescriptionOf(contentMatcher);
        }
    }

    public static FileContainsSvgMatcher isSvgFile() {
        return new FileContainsSvgMatcher(null);
    }

    public static FileContainsSvgMatcher isSvgFileThat(Matcher<? super SVGDocument> contentMatcher) {
        return new FileContainsSvgMatcher(contentMatcher);
    }
}
