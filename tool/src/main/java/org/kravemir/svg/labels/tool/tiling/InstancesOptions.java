package org.kravemir.svg.labels.tool.tiling;

import org.apache.commons.csv.CSVFormat;
import org.kravemir.svg.labels.tiling.LabelGroup;
import org.kravemir.svg.labels.tool.cli.converter.CSVFormatConverter;
import org.kravemir.svg.labels.tool.model.ReferringLabelGroup;
import org.kravemir.svg.labels.tooling.Loader;
import picocli.CommandLine;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;

public class InstancesOptions {

    @CommandLine.Option(
            names = "--instance-json", paramLabel = "FILE",
            description = "Path to JSON file containing values for single instance"
    )
    private File instanceJsonFile;

    @CommandLine.Option(
            names = "--instances-json", paramLabel = "FILE",
            description = "Path to JSON file containing array of instances (can be used in combination with --dataset-json)"
    )
    private File instancesJsonFile;

    @CommandLine.Option(
            names = "--dataset-json", paramLabel = "FOLDER",
            description = "Path to folder containing JSON files for instances"
    )
    private Path datasetJsonPath;

    @CommandLine.Option(
            names = "--dataset-csv", paramLabel = "FILE",
            description = "Path to CSV file containing instances"
    )
    private Path datasetCSVPath;

    @CommandLine.Option(
            names = "--dataset-csv-format", paramLabel = "FORMAT",
            converter = CSVFormatConverter.class, defaultValue = "Default",
            description = "Sets format for parsing CSV dataset (available options: Default, Excel, InformixUnload, InformixUnloadCsv, MySQL, PostgreSQLCsv, PostgreSQLText, RFC4180, TDF)"
    )
    private CSVFormat datasetCSVFormat;

    @CommandLine.Option(
            names = "--instance", paramLabel = "KEY",
            description = "Key of instance to be rendered"
    )
    private String instance;

    private final Loader loader;

    public InstancesOptions() {
        loader = new Loader();
    }

    public boolean hasInstances() {
        return instancesJsonFile != null || instanceJsonFile != null || datasetCSVPath != null;
    }

    public List<LabelGroup.Instance> loadInstances() throws IOException {
        List<LabelGroup.Instance> instances;

        if (instancesJsonFile != null) {
            instances = loadJsonInstances();
        } else if (instanceJsonFile != null) {
            instances = loadInstance();
        } else if (datasetCSVPath != null) {
            instances = loadInstanceFromCSV();
        } else {
            instances = singletonList(LabelGroup.Instance.newBuilder().setCount(1).setFillPage(true).build());
        }

        return instances;
    }

    private List<LabelGroup.Instance> loadInstance() throws IOException {
        HashMap<String, String> values = loader.loadInstance(instanceJsonFile);

        return singletonList(LabelGroup.Instance.newBuilder().setCount(1).setFillPage(true).setInstanceContent(values).build());
    }

    private List<LabelGroup.Instance> loadJsonInstances() throws IOException {
        ReferringLabelGroup.Instance[] instances = loader.loadInstances(instancesJsonFile);

        return Arrays.stream(instances)
                .map(this::mapInstance)
                .collect(Collectors.toList());
    }

    private List<LabelGroup.Instance> loadInstanceFromCSV() throws IOException {
        Map<String, Map<String, String>> instances = loader.loadInstances(datasetCSVPath, datasetCSVFormat);
        Map<String, String> instanceData = instances.get(instance);

        return singletonList(LabelGroup.Instance.newBuilder().setCount(1).setFillPage(true).setInstanceContent(instanceData).build());
    }

    private LabelGroup.Instance mapInstance(ReferringLabelGroup.Instance instance) {
        LabelGroup.Instance.Builder builder = LabelGroup.Instance.newBuilder();

        if (instance.getInstanceContent() != null && !isEmpty(instance.getInstanceContentRef())) {
            // TODO: cleanup / think about this, override?
            throw new RuntimeException("Both ref and content are present");
        } else if (instance.getInstanceContent() != null) {
            builder.setInstanceContent(instance.getInstanceContent());
        } else if (!"".equals(instance.getInstanceContentRef())) {
            builder.setInstanceContent(loadInstanceContent(instance.getInstanceContentRef()));
        } else {
            // TODO: cleanup / think about this, not content
            throw new RuntimeException("None of ref and content are present");
        }

        builder.setCount(instance.getCount());
        builder.setFillPage(instance.getFillPage());

        return builder.build();
    }

    private boolean isEmpty(String value) {
        return value == null || "".equals(value);
    }

    private Map<String, String> loadInstanceContent(String name) {

        try {
            return loader.loadInstance(datasetJsonPath.resolve(name + ".json").toFile());
        } catch (IOException e) {
            // TODO: error handling
            throw new RuntimeException(e);
        }
    }
}
