package org.kravemir.svg.labels.tool.cli.converter;

import org.apache.commons.csv.CSVFormat;
import picocli.CommandLine;

public class CSVFormatConverter implements CommandLine.ITypeConverter<CSVFormat> {
    @Override
    public CSVFormat convert(String value) {
        return CSVFormat.valueOf(value);
    }
}
