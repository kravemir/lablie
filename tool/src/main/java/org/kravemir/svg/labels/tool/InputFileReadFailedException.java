package org.kravemir.svg.labels.tool;

import java.io.File;

public class InputFileReadFailedException extends RuntimeException {

    private final String parameter;
    private final File file;

    public InputFileReadFailedException(String parameter, File file, Throwable cause) {
        super(cause);

        this.parameter = parameter;
        this.file = file;
    }

    @Override
    public String getMessage() {
        return String.format("Input %s \"%s\" can't be read, because: %s", parameter, file.getPath(), getCause().getMessage());
    }
}
