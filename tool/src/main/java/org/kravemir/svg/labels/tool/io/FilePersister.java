package org.kravemir.svg.labels.tool.io;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class FilePersister {

    public void save(File target, List<String> result) throws IOException {
        if(result.size() == 1) {
            FileUtils.writeStringToFile(target, result.get(0));
        } else {
            String path = target.getPath();
            int lastDot = path.lastIndexOf('.');
            String base = path.substring(0,lastDot);
            String extension = path.substring(lastDot);

            for(int i = 0; i < result.size(); i++) {
                FileUtils.writeStringToFile(
                        new File(base + "." + i + extension),
                        result.get(i)
                );
            }
        }
    }
}
