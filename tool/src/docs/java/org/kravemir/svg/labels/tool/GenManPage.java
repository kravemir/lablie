package org.kravemir.svg.labels.tool;

import picocli.CommandLine;

public class GenManPage {

    public static void main(String[] args) {
        CommandLine commandLine = ToolRunner.constructCommandLine();
        ManPageGenerator generator = new ManPageGeneratorFactory().create();

        generator.generateManPage(System.out, commandLine.getCommandSpec());
    }
}
