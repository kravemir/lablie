package org.kravemir.svg.labels.tool;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

/**
 * Factory to construct {@link ManPageGenerator} based on current generation environment.
 */
public class ManPageGeneratorFactory {

    private static final Supplier<LocalDate> DEBIAN_CHANGELOG_DATE = () -> {
        String epochString = System.getenv("SOURCE_DATE_EPOCH");
        if (epochString != null) {
            long epoch = Long.parseLong(epochString);

            return LocalDateTime.ofEpochSecond(epoch, 0, ZoneOffset.UTC).toLocalDate();
        } else {
            return null;
        }
    };

    private static final List<Supplier<LocalDate>> MANPAGE_DATE_GENERATORS = Arrays.asList(
            DEBIAN_CHANGELOG_DATE,
            LocalDate::now
    );

    public ManPageGenerator create() {
        return new ManPageGenerator(getManPageDate());
    }

    private LocalDate getManPageDate() {
        for (Supplier<LocalDate> supplier : MANPAGE_DATE_GENERATORS) {
            LocalDate result = supplier.get();

            if (result != null) {
                return result;
            }
        }

        throw new RuntimeException("Determination of manpage date failed");
    }
}
