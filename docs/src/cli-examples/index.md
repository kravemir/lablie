# CLI Examples

Summary of examples:

| Example                                                      | Description                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [01-tile-label](01-tile-label/)                     | Create SVG and PDF for print containing tiled label          |
| [02-tile-label-with-instancing](02-tile-label-with-instancing/) | Create SVG(s) and PDF(s) for print containing tiled label filled with data of corresponding instances (products) |
| [91-batch-instacing-using-shell](91-batch-instacing-using-shell/) | Create instanced SVG(s) and PDF(s) for print, in batch, using `bash` script. |
| [92-batch-instacing-using-makefile](92-batch-instacing-using-makefile/) | Create instanced PDF(s) for print, in batch, using `Makefile`. Supports incremental builds, thanks to `make` functionality. |
