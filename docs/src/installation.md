# Installation


## Snap package

The tool is packaged as [snap package at snapcraft.io][snapcraft-io-package], and can be installed with:

```bash
sudo snap install lablie
```

## Native distribution package

Not packaged, yet.

[snapcraft-io-package]: https://snapcraft.io/lablie

## From sources

No manual way, yet.
