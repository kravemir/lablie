# `lablie project` command

TODO: add a brief information 

## Reference 

Output of the `lablie project --help`: 

```
{{#include project.help.txt}}
```

More detailed reference will be added in future.


## Related examples

In [CLI examples](../../cli-examples) chapter, following example show usage of the `lablie project` command:

* [#11 - example project](../../cli-examples/11-example-project)

