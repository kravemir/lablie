# `lablie tile` command

The command `tile` creates SVG document with tiled labels based on paper's and label's specifications.

## Reference 

See output of the `lablie tile --help`: 

```
{{#include tile.help.txt}}
```

More detailed reference will be added in future.

## Related examples

In [CLI examples](../../cli-examples) chapter, following examples show usage of the `lablie tile` command:

* [#01 - tile label](../../cli-examples/01-tile-label)
* [#02 - tile label with instancing](../../cli-examples/02-tile-label-with-instancing)

