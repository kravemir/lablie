package org.kravemir.svg.labels.batik;

import org.kravemir.svg.labels.instancing.LabelTemplateDescriptor;
import org.w3c.dom.svg.SVGDocument;

import java.util.Collections;

public class InstancingTemplateFactory {

    public InstancingTemplate create(String svg, LabelTemplateDescriptor templateDescriptor) {
        final SVGDocument document = RenderingUtils.parseSVG(svg);

        if(templateDescriptor == null) {
            templateDescriptor = LabelTemplateDescriptor.newBuilder()
                    .setAttributes(Collections.emptyList())
                    .setContentReplaceRules(Collections.emptyList())
                    .build();
        }

        return new InstancingTemplate(document, templateDescriptor);
    }
}
