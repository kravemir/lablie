package org.kravemir.svg.labels.batik;

import org.kravemir.svg.labels.instancing.InstanceRenderer;
import org.kravemir.svg.labels.tiling.DocumentRenderOptions;
import org.kravemir.svg.labels.tiling.LabelGroup;
import org.kravemir.svg.labels.tiling.TiledPaper;
import org.kravemir.svg.labels.tiling.TiledLabelDocumentsRenderer;
import org.w3c.dom.svg.SVGDocument;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * {@link BatikTiledLabelDocumentsRenderer} renders documents with tiled labels, and is intended to be reentrant.
 *
 * Implementation of {@link InstanceRenderer} using SVG DOM implementation provided by Apache Batik SVG Toolkit
 */
public class BatikTiledLabelDocumentsRenderer implements TiledLabelDocumentsRenderer {

    /*
        TODO: scaling, exceptions
     */

    @Override
    public List<String> render(TiledPaper paper, List<LabelGroup> labels, DocumentRenderOptions options) {
        return renderAsSVGDocument(paper, labels, options)
                .stream()
                .map(RenderingUtils::documentToString)
                .collect(Collectors.toList());
    }

    private Collection<SVGDocument> renderAsSVGDocument(TiledPaper paper, List<LabelGroup> labels, DocumentRenderOptions options) {

        InstancingTemplateFactory instancingTemplateFactory = new InstancingTemplateFactory();
        TiledLabelDocumentsBuilder builder = new TiledLabelDocumentsBuilder(paper, options);

        for (LabelGroup l : labels) {
            InstancingTemplate instancingTemplate = instancingTemplateFactory.create(l.getTemplate(), l.getTemplateDescriptor());

            for (LabelGroup.Instance instance : l.getInstances()) {
                String templateSVG = RenderingUtils.documentToString(instancingTemplate.instantiate(instance.getInstanceContent()));
                LabelTemplate template = LabelTemplate.create(templateSVG);

                for (int n = 0; n < instance.getCount(); n++) {
                    builder.placeLabel(template);
                }

                if (instance.getFillPage()) {
                    while (builder.isSpaceLeftOnCurrentPage()) {
                        builder.placeLabel(template);
                    }
                }
            }
        }

        return builder.getDocuments();
    }

    @Override
    public String renderSinglePageWithLabel(TiledPaper paper, String SVG) {
        LabelGroup l = LabelGroup.newBuilder()
                .setTemplate(SVG)
                .addAllInstances(Collections.singletonList(LabelGroup.Instance.newBuilder().setCount(1).setFillPage(true).build()))
                .build();
        return render(paper, Collections.singletonList(l), DocumentRenderOptions.newBuilder().build()).get(0);
    }
}
