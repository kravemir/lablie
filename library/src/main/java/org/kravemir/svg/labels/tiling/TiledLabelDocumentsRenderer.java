package org.kravemir.svg.labels.tiling;

import org.kravemir.svg.labels.annotations.ToBePublicApi;

import java.util.List;

@ToBePublicApi
public interface TiledLabelDocumentsRenderer {

    /**
     * Renders labels
     *
     * @param paper   specification of a paper
     * @param labels  specification of labels to be rendered
     * @param options rendering options
     * @return generated pages as a SVG
     */
    @ToBePublicApi
    List<String> render(TiledPaper paper, List<LabelGroup> labels, DocumentRenderOptions options);

    /**
     * Renders one page filled by single label
     *
     * @param paper specification of a paper
     * @param SVG   {@link String} containing <code>SVG</code> of label to be rendered
     * @return generate page
     */
    String renderSinglePageWithLabel(TiledPaper paper, String SVG);
}
