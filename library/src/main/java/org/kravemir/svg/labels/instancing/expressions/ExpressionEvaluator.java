package org.kravemir.svg.labels.instancing.expressions;

import org.apache.commons.jexl3.*;

import java.util.Collections;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExpressionEvaluator {

    private Pattern p = Pattern.compile(
            "\\{\\{(.*?)}}"
    );

    private JexlEngine jexl = new JexlBuilder().create();

    public String populateValuePlaceholders(String value, Map<String,String> variables) {
        Matcher m = p.matcher(value);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            String expression = m.group(1);
            Object replacement = evaluateExpressionWithJEXL(expression, variables);

            m.appendReplacement(sb, String.valueOf(replacement));
        }
        m.appendTail(sb);
        return sb.toString();
    }

    public Object evaluateExpressionWithJEXL(String expression, Map<String,String> variables) {
        // Create an expression
        JexlExpression e = jexl.createExpression( expression );

        // Create a context and add data
        final JexlContext jc = new MapContext();
        jc.set("instance", Collections.unmodifiableMap(variables));

        // Now evaluate the expression, getting the result
        return e.evaluate(jc);
    }
}
