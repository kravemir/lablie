package org.kravemir.svg.labels.batik;

import org.apache.batik.anim.dom.SVGDOMImplementation;
import org.kravemir.svg.labels.tiling.DocumentRenderOptions;
import org.kravemir.svg.labels.tiling.TiledPaper;
import org.kravemir.svg.labels.tiling.TilePositionGenerator;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * {@link TiledLabelDocumentsBuilder} builds SVG documents with tiled labels based on specified {@link TiledPaper} and {@link DocumentRenderOptions}
 */
public class TiledLabelDocumentsBuilder {
    private final DocumentRenderOptions options;

    private final TilePositionGenerator positionGenerator;
    private final List<SVGDocument> documents;
    private SVGDocument document;

    public TiledLabelDocumentsBuilder(TiledPaper p, DocumentRenderOptions options) {
        this.options = options;

        positionGenerator = new TilePositionGenerator(p);
        documents = new ArrayList<>();
    }

    private void startDocument() {
        DOMImplementation impl = SVGDOMImplementation.getDOMImplementation();
        document = (SVGDocument) impl.createDocument(SVGDOMImplementation.SVG_NAMESPACE_URI, "svg", null);
        Element root = document.getDocumentElement();

        //set page size
        root.setAttributeNS(null, "width", RenderingUtils.length(getPaperWidth()));
        root.setAttributeNS(null, "height", RenderingUtils.length(getPaperHeight()));
        //root.setAttributeNS(null, "viewBox", "0 0 " + lw.getValueAsString() + " " + lh.getValueAsString());

        if (options.getRenderPageBorders())
            root.appendChild(RenderingUtils.createRect(document, 0, 0, getPaperWidth(), getPaperHeight()));

        positionGenerator.start();
    }


    public void placeLabel(LabelTemplate template) {
        if (!isSpaceLeftOnCurrentPage()) {
            startDocument();
            documents.add(document);
        }

        LabelPosition position = calculateLabelPosition(template);

        placeLabelClone(template, position);

        if (options.getRenderTileBorders())
            renderTileBorders();

        if (options.getRenderLabelBorders())
            renderLabelBorders(position);

        positionGenerator.nextPosition();
    }

    private void placeLabelClone(LabelTemplate template, LabelPosition position) {
        Element label = (Element) template.templateRoot.cloneNode(true);
        document.adoptNode(label);
        label.setAttributeNS(null, "x", RenderingUtils.length(position.x));
        label.setAttributeNS(null, "y", RenderingUtils.length(position.y));
        label.setAttributeNS(null, "width", RenderingUtils.length(template.labelW));
        label.setAttributeNS(null, "height", RenderingUtils.length(template.labelH));
        document.getRootElement().appendChild(label);
    }

    private void renderTileBorders() {
        document.getRootElement().appendChild(RenderingUtils.createRect(
                document,
                getX(),
                getY(),
                positionGenerator.getTileWidth(),
                positionGenerator.getTileHeight())
        );
    }

    private void renderLabelBorders(LabelPosition position) {
        document.getRootElement().appendChild(RenderingUtils.createRect(
                document,
                position.x,
                position.y,
                position.width,
                position.height)
        );
    }

    public Collection<SVGDocument> getDocuments() {
        return documents;
    }

    public boolean isSpaceLeftOnCurrentPage() {
        return document != null && !positionGenerator.isFull();
    }

    private double getX() {
        return positionGenerator.getX();
    }

    private double getY() {
        return positionGenerator.getY();
    }

    private double getPaperWidth() {
        return positionGenerator.getPaperWidth();
    }

    private double getPaperHeight() {
        return positionGenerator.getPaperHeight();
    }

    private LabelPosition calculateLabelPosition(LabelTemplate template) {
        return new LabelPosition(
                getX(),
                getY(),
                (positionGenerator.getTileWidth() - template.labelW) / 2,
                (positionGenerator.getTileHeight() - template.labelH) / 2
        );
    }

    private static class LabelPosition {
        final double x, y, width, height;

        public LabelPosition(double x, double y, double width, double height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
    }
}
