package org.kravemir.svg.labels.batik;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import static org.kravemir.svg.labels.batik.RenderingUtils.parseSVG;

class LabelTemplate {
    Element templateRoot = null;
    double labelW = 0, labelH = 0;

    public static LabelTemplate create(String svg) {
        LabelTemplate template = new LabelTemplate();
        template.load(svg);
        return template;
    }

    private void load(String svg) {
        Document templateDoc = parseSVG(svg);
        if (templateDoc != null) {
            templateRoot = templateDoc.getDocumentElement();
            labelW = RenderingUtils.length(templateRoot.getAttributeNS(null, "width"));
            labelH = RenderingUtils.length(templateRoot.getAttributeNS(null, "height"));
        }
    }
}
