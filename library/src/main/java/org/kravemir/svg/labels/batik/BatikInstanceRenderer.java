package org.kravemir.svg.labels.batik;

import org.kravemir.svg.labels.instancing.InstanceRenderer;
import org.kravemir.svg.labels.instancing.LabelTemplateDescriptor;
import org.w3c.dom.svg.SVGDocument;

import java.util.Map;

/**
 * Implementation of {@link InstanceRenderer} using SVG DOM implementation provided by Apache Batik SVG Toolkit
 */
public class BatikInstanceRenderer implements InstanceRenderer {

    private final InstancingTemplateFactory instancingTemplateFactory = new InstancingTemplateFactory();

    @Override
    public String render(String svgTemplate,
                         LabelTemplateDescriptor templateDescriptor,
                         Map<String, String> instanceContent) {

        InstancingTemplate template = instancingTemplateFactory.create(svgTemplate, templateDescriptor);

        SVGDocument result = template.instantiate(instanceContent);

        return RenderingUtils.documentToString(result);
    }
}
