package org.kravemir.svg.labels.tiling;

import org.kravemir.svg.labels.instancing.LabelTemplateDescriptor;

class TiledPaper {
    double with;
    double height;

    double tile_offset_x;
    double tile_offset_y;
    double tile_width;
    double tile_height;
    double tile_delta_x;
    double tile_delta_y;
}

class LabelGroup {

    class Instance {
        int32 count;
        bool fillPage;
        map<string,string> instanceContent;
    }

    string template;
    LabelTemplateDescriptor templateDescriptor;
    repeated Instance instance;
}

class DocumentRenderOptions {
    bool renderPageBorders;
    bool renderTileBorders;
    bool renderLabelBorders;
}

